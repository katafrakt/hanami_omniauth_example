require 'spec_helper'
require_relative '../../../../apps/web/controllers/users/register'

describe Web::Controllers::Users::Register do
  let(:action) { Web::Controllers::Users::Register.new }
  let(:params) { Hash[] }

  it 'is successful' do
    response = action.call(params)
    response[0].must_equal 200
  end
end
