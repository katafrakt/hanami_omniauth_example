module Web::Controllers::Session
  class Failure
    include Web::Action

    def call(params)
      flash[:error] = 'Invalid credentials'
      redirect_to '/sign_in'
    end
  end
end
