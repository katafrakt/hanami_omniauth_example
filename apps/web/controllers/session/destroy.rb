module Web::Controllers::Session
  class Destroy
    include Web::Action
    include Web::Authentication

    def call(_params)
      warden.logout
      redirect_to '/'
    end
  end
end
