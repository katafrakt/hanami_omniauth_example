module Web::Views::Users
  class SignIn
    include Web::View

    def form
      form_for :user, '/auth/hanami/callback', class: 'pure-form pure-form-aligned' do
        fieldset do
          div do
            div class: 'pure-control-group' do
              label :email
              text_field :email
            end
            div class: 'pure-control-group' do
              label :password
              password_field :password
            end
          end
          div class: 'pure-controls' do
            submit 'Sign in!', class: 'pure-button pure-button-primary'
            a 'Sign in with Github', href: '/auth/github', class: 'pure-button'
          end
        end
      end
    end
  end
end
