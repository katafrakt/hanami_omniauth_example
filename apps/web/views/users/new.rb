module Web::Views::Users
  class New
    include Web::View

    def form
      form_for :user, routes.register_path, class: 'pure-form pure-form-aligned' do
        fieldset do
          div class: 'pure-control-group' do
            label :name
            text_field :name
          end
          div class: 'pure-control-group' do
            label :email
            text_field :email, type: 'email'
          end
          div class: 'pure-control-group' do
            label :password
            password_field :password
          end
          div class: 'pure-controls' do
            submit 'Register', class: 'pure-button pure-button-primary'
            a 'Back', href: '/', class: 'pure-button'
          end
        end
      end
    end
  end
end
